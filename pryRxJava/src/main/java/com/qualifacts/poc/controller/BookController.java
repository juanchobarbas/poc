package com.qualifacts.poc.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import com.qualifacts.poc.service.BookService;
import com.qualitacts.poc.bean.Book;
import rx.Observable;

@RestController
@RequestMapping(value="/books")
public class BookController {
	
	@Autowired
	private BookService	bookService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Book>> getBooks() throws Exception {		
		return new ResponseEntity<>(bookService.getBooks(), HttpStatus.OK);
	}

	@RequestMapping(value = "/{customerId}", method = RequestMethod.GET)
	public DeferredResult<List<Book>> getFavoriteBooks(@PathVariable String customerId) throws Exception {		
		final DeferredResult<List<Book>> favoriteBooksResult = new DeferredResult<>();
		Observable<List<Book>> obsBooks = bookService.getFavoriteBooks();
		System.out.println("Executing Async Process For Customer: "+ customerId);
		obsBooks.subscribe(favoriteBooksResult::setResult);
		return favoriteBooksResult;
	}

}
