package com.qualifacts.poc.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import com.qualitacts.poc.bean.Book;

import rx.Observable;
import rx.schedulers.Schedulers;

@Service
public class BookService {

	public List<Book> getBooks() {
		return createBooks();
	}
	
    public List<Book> getFavoriteBooks(String bookType) {
        try {        	       
        	System.out.println("Init Get Favorite Book for Type :" + bookType);
			Thread.sleep(2000);
			System.out.println("Completed Get Favorite Book for Type :" + bookType);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}        
        return createBooks();
    }

	public Observable<List<Book>> getFavoriteBooks() {
		Observable<List<Book>> favoriteThrillerBooks = Observable.fromCallable(() -> getFavoriteBooks("thriller")).subscribeOn(Schedulers.io());
		Observable<List<Book>> favoriteFayriBooks = Observable.fromCallable(() -> getFavoriteBooks("fairy")).subscribeOn(Schedulers.io());
		return Observable.merge(favoriteThrillerBooks,favoriteFayriBooks);
	}
	
	private List<Book> createBooks() {
		List<Book> books = new ArrayList<>();

		books.add(new Book("Lord of the Rings", "Author 1"));
		books.add(new Book("The dark elf", "Author 2"));
		books.add(new Book("Eclipse Introduction", "Author 3"));
		books.add(new Book("History book", "Author 4"));
		books.add(new Book("Der kleine Prinz", "Author 5"));
		books.add(new Book("7 habits of highly effective people", "Author 6"));
		books.add(new Book("Other book 1", "Author 7"));
		books.add(new Book("Other book 2", "Author 8"));
		return books;
	}
}
