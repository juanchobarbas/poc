package com.gigy.config;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import com.gigy.model.Scope;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
	// needs to be a separate config in order to be used in unit test with custom slices
	
	   @Override
	    public void configure(HttpSecurity http) throws Exception {
		   List<Scope> listScope=getDynamicScope();
		    for(Scope s : listScope) {
		      http.authorizeRequests()
		        .antMatchers(s.getPath())
		        .access("#oauth2.hasScope('"+s.getName()+"')");
		    }
	    }
	   	
	   @Override
	    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		   	 //Which resource?
	         resources.resourceId("api");
	    }
	   
	   //TODO Search into DataBase.
	   private List<Scope> getDynamicScope() {
		   List<Scope> listScope = new ArrayList<>();		   
		   Scope scopePeople = new Scope();
		   scopePeople.setName("people");
		   scopePeople.setPath("/api/people/**");		
		   
		   Scope scopeParty = new Scope();
		   scopeParty.setName("party");
		   scopeParty.setPath("/api/parties/**");
		   
		   listScope.add(scopePeople);	
		   listScope.add(scopeParty);
		   return listScope;
	   }
}

