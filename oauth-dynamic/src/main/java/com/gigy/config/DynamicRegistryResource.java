package com.gigy.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

public class DynamicRegistryResource implements BeanDefinitionRegistryPostProcessor {

	@Override
	public void postProcessBeanDefinitionRegistry(final BeanDefinitionRegistry registry) throws BeansException {

		
		   //Add Fhir Resource Bean.
	       registry.registerBeanDefinition("dynamicFhirResource",
	               BeanDefinitionBuilder
	               .rootBeanDefinition(ResourceConfigBean.class)
	               .addPropertyValue("resourceId", "fhir")
	               .addPropertyValue("order", 29)
	               .setInitMethodName("initialize")
	               .getBeanDefinition());
	       
		   //Add Other Resource Bean.
	       registry.registerBeanDefinition("dynamicOtherResource",
	               BeanDefinitionBuilder
	               .rootBeanDefinition(ResourceConfigBean.class)
	               .addPropertyValue("resourceId", "other")
	               .addPropertyValue("order", 30)
	               .setInitMethodName("initialize")
	               .getBeanDefinition());

	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		
	}	
	
}