package com.gigy.config;

import java.util.Arrays;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfiguration;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

public class ResourceConfigBean extends ResourceServerConfiguration {

	private int order;
	private String resourceId;

	public void initialize() throws Exception {
		//It´s not setting the configures for the first resource.
		this.setConfigurers(Arrays.<ResourceServerConfigurer>asList(new ResourceServerConfigurerAdapter() {

			@Override
			public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
				resources.resourceId(resourceId);
			}

			@Override
			public void configure(HttpSecurity http) throws Exception {
				 http	        
		            .antMatcher("/"+resourceId+"/**")
			        .authorizeRequests().anyRequest().authenticated();
			}

		}));
		this.setOrder(order);
	}
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
}