package com.gigy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ResourceServerConfig {
	
	@Bean
	protected DynamicRegistryResource registryResourceBean() {
		return new DynamicRegistryResource();
	}
	
}