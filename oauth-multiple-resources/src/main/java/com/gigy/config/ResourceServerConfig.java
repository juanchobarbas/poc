package com.gigy.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfiguration;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
public class ResourceServerConfig {
	
	@Bean
	protected ResourceServerConfiguration partyResources() {
		ResourceServerConfiguration resource = new ResourceServerConfiguration() {
			// Switch off the Spring Boot @Autowired configurers
			public void setConfigurers(List<ResourceServerConfigurer> configurers) {
				super.setConfigurers(configurers);
			}

			@Override
			public int getOrder() {
				return 29;
			}
		};
		resource.setConfigurers(Arrays.<ResourceServerConfigurer>asList(new ResourceServerConfigurerAdapter() {
			@Override
			public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
				 resources.resourceId("fhir");
			}

			@Override
			public void configure(HttpSecurity http) throws Exception {
				 http	        
		            .antMatcher("/fhir/**")
			        .authorizeRequests().anyRequest().authenticated();
			}
		}));
		return resource;
	}
	
	
	@Bean
	protected ResourceServerConfiguration peopleSources() {
		ResourceServerConfiguration resource = new ResourceServerConfiguration() {
			// Switch off the Spring Boot @Autowired configurers
			public void setConfigurers(List<ResourceServerConfigurer> configurers) {
				super.setConfigurers(configurers);
			}

			@Override
			public int getOrder() {
				return 30;
			}
		};
		resource.setConfigurers(Arrays.<ResourceServerConfigurer>asList(new ResourceServerConfigurerAdapter() {
			@Override
			public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
				 resources.resourceId("other");
			}

			@Override
			public void configure(HttpSecurity http) throws Exception {
				 http	        
		            .antMatcher("/other/**")
			        .authorizeRequests().anyRequest().authenticated();
			}
		}));
		return resource;
	}
}