package com.gigy.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gigy.model.Party;

@Repository
public interface PartyRepository extends CrudRepository<Party, Long> {
	
	/* (non-Javadoc)
	 * @see org.springframework.data.repository.CrudRepository#findAll()
	 */
	Collection<Party> findAll();	
}
